<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "bonus".
 *
 * @property integer $id
 * @property integer $userId
 * @property integer $reasonId
 * @property integer $amount
 */
class Bonus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'reasonId', 'amount'], 'required'],
            [['userId', 'reasonId', 'amount'], 'integer'],
			 ['amount', 'compare', 'compareValue' => 100, 'operator' => '>='],
			 ['amount', 'compare', 'compareValue' => 1000, 'operator' => '<='],
			//[['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
		];			
		
    }

		public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'reasonId' => 'Reason ID',
            'amount' => 'Amount',
			'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
		
	
	////Add for dropDown
	public static function getBonus()
	{
		$allBouns = self::find()->all();
		$allBonusArray = ArrayHelper::
					map($allBouns, 'id', 'reasonId');
		return $allBounsArray;						
	}

	////Add for dropDown
	public function getBonusItem()
	{
      return $this->hasOne(Bonus::className(), ['id' => 'reasonId']);
	}
	
}
