<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Lead;
use app\models\Bonus;



/* @var $this yii\web\View */
/* @var $model app\models\Bonus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bonus-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'userId')->textInput() ?>
	
	<?php/* if (\Yii::$app->user->can('updateBonus')) { ?>
	<?= $form->field($model, 'reasonId')->dropDownList(Bonus::getBonus()) ?>    
	<?php } */?>
	
	<?= $form->field($model, 'reasonId')->textInput() ?>

  
    <?= $form->field($model, 'amount')->textInput() ?>
	


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
